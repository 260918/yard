# Item order

[![Build Status](https://travis-ci.org/micro-frontends-demo/restaurant-order.svg?branch=master)](https://travis-ci.org/micro-frontends-demo/restaurant-order)

A micro frontend for ordering food from a yard

# Getting started

1. Clone the repo
2. `yarn install`
3. `yarn start`

You'll also need to run the [`content`](hhttps://gitlab.com/260918/content) server.
Open the app at `/restaurant/2` to see restaurant number 2, for example.

This will run the app on its own, outside of the container. This is useful while
you're tweaking logic and styling, but you'll usually then need to integrate and
test it with the rest of the application. So you should also run:

- the [`container`](https://gitlab.com/260918/container) application
- the [`supply-request`](https://gitlab.com/260918/supply-request) micro frontend

# Run the tests

- `yarn test`
