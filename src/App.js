import React, {useEffect} from 'react';
import { Router, Route } from 'react-router-dom';
import styled from 'styled-components';
import { createBrowserHistory } from 'history';
import Item from './Item';

const MainColumn = styled.div`
  max-width: 1150px;
  margin: 0 auto;
  padding: 20px;
`;

const defaultHistory = createBrowserHistory();

const App = ({history = defaultHistory}) => {

    console.log(JSON.stringify(history, null, 2));
    console.log(JSON.stringify(defaultHistory, null, 2));
    useEffect(() => {
        // console.log(JSON.stringify(props.history, null, 2));
    }, []);

    return (
        <Router history={history}>
            <MainColumn>
                <Route exact path="/item/:id" component={Item}/>
            </MainColumn>
        </Router>
    );
};

export default App;
