import React from 'react';
import styled from "styled-components";

const textarea = styled.div`
  padding: 10px
`;

class Item extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      error: false,
      item: null,
    };
  }

  componentDidMount() {
    const host = process.env.REACT_APP_CONTENT_HOST;
    const id = this.props.match.params.id;
    // console.log(id);

    fetch(`${host}/itemList/${id}.json`)
      .then(result => result.json())
      .then(item => {
        this.setState({
          item: {
            ...item,
            locate: item.locate
          },
          loading: false,
        });
      })
      .catch(() => {
        this.setState({ loading: false, error: true });
      });
  }

  render() {

    const {item} = this.state;

    if (this.state.loading) {
      return 'Loading';
    }
    if (this.state.error) {
      return 'Sorry, but that restaurant is currently unavailable.';
    }

    return <textarea value={item.locate} ></textarea>;
  }
}

export default Item;
