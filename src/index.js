import 'react-app-polyfill/ie11';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { unregister } from './registerServiceWorker';

window.renderYard = (containerId, history) => {

  // console.log(JSON.stringify(history, null, 2));
  ReactDOM.render(
    <App history={history}/>,
    document.getElementById(containerId),
  );
  unregister();
};

window.unmountYard = containerId => {
  ReactDOM.unmountComponentAtNode(document.getElementById(containerId));
};
